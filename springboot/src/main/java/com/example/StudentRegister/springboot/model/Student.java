package com.example.StudentRegister.springboot.model;

import javax.persistence.*;

@Entity
@Table(name ="student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "id",unique = true)
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "phone")
    private String phone;
    @Column(name = "address")
    private String address;
    @ManyToOne
    private StudentType studentType;
    public StudentType getStudentType(){
        return studentType;
    }
    public void setStudentType(StudentType studentType){
        this.studentType=studentType;
    }
   public Student(){
   }
    public Student(int id,String name,String phone,String address){
        this.id=id;
        this.name=name;
        this.phone=phone;
        this.address=address;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + this.id +
                ", name='" + this.name + '\'' +
                ", name='" + this.phone + '\'' +
                ", address='" + this.address + '\'' +
                '}';
    }
}
