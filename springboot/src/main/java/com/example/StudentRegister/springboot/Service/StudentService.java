package com.example.StudentRegister.springboot.Service;

import com.example.StudentRegister.springboot.Repository.StudentRepository;
import com.example.StudentRegister.springboot.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {
    private StudentRepository studentRepository;
    //private StudentRepository repository;
    //StudentRepository obj = new StudentRepository();
    @Autowired
    public StudentService(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }
    public void addStudent(Student student){
        studentRepository.save(student);
    }
    public List<Student> getStudentList(){

        return (List<Student>) this.studentRepository.findAll();
    }
    public Student findById(Integer id){
        return this.studentRepository.findById(id).get();
    }
    public void deleteById(Integer id){
        this.studentRepository.deleteById(id);
    }
    public List<Student> findByName(String Name){
        return this.studentRepository.findByName(Name);
    }
}
