package com.example.StudentRegister.springboot.Repository;

import com.example.StudentRegister.springboot.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public interface StudentRepository extends CrudRepository<Student,  Integer> {
  List<Student> findByName(String Name);

}
